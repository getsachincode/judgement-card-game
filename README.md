# Welcome to Judgement!
Judgement is a card game played with 3 players or higher. The objective of the game is for each player to predict how many hands/tricks they will make (after the cards have been dealt) and then exactly make that number of tricks.

Try the game here: [https://gentle-complex-colt.glitch.me/](https://gentle-complex-colt.glitch.me/)

## Why did I create this application?
During the COVID-19 lockdown, my entire family came together in the evenings and played this game. Now that my brother has returned to college and my school has opened up too, I wanted to keep the play alive, so I created an online version of the game. The cards are randomly dealt and we play the game exactly the way we would have, but on our phones! Now I can play the game with family and friends without using physical cards or a paper scoreboard.

## Rules of the Game:

 - To create a new game board, enter your name and choose the number of players for the game (3-8). After creating the board, you have to wait for other players to join your board. You can then copy the link to send to your friends to join this board or you could send them the 4 character alphanumeric code. Your friends can join the board by going to the link you sent and entering their name. The game code link will be auto-populated or it could be typed in. Note: If the game code is not entered, it will create a new game board and not allow your friend to join your board. When the board is full, the first set of cards is dealt and the game begins.
    
    >  Start of Game
    
    >![Start the game by picking your name](https://bitbucket.org/getsachincode/judgement-card-game/raw/dfdbe0ed0d5b0832f7fbe57dd28acb3abeb73d57/screenshots/startGame.png) ![Waiting for players](https://bitbucket.org/getsachincode/judgement-card-game/raw/17bf438c8a0c0611978e0d26bcf0ee6fbff5e314/screenshots/waitForStart.png) ![Join the game by entering the code](https://bitbucket.org/getsachincode/judgement-card-game/raw/dfdbe0ed0d5b0832f7fbe57dd28acb3abeb73d57/screenshots/joinGame.png)

 - Players are dealt an equal number of cards at the start of a round. *For example* if there are 4 players, there are 13 cards dealt to each player in the first round.

 - There is a strong suit (trump) assigned at the beginning of the round. After seeing their cards, the players sequentially bid the exact number of tricks they want to make during the round. The last player in the round is not allowed to bid a number where the sum of all the bids equals the number of cards dealt per player.

    >  Bid the exact number of tricks you want to make

    >![Bid](https://bitbucket.org/getsachincode/judgement-card-game/raw/17bf438c8a0c0611978e0d26bcf0ee6fbff5e314/screenshots/bid.png) ![Confirm Bid](https://bitbucket.org/getsachincode/judgement-card-game/raw/17bf438c8a0c0611978e0d26bcf0ee6fbff5e314/screenshots/bidConfirm.png)

- After the bidding, the first player to bid starts by playing a card. The play continues clockwise until all players have played a card. All subsequent players are required to play a card from the original suit played by the first player, unless they don't have a card from the suit. The ones who don't have a card from the original suit, may choose to trump this sequence to try to win the trick. The winner of the trick/hand is the one who plays the highest card in the original suit played or the highest trump played. The winner then starts the next trick/hand and this continues until all cards have been played.

    >  Play a card. The player winning the trick so far has the dark background.

    >![Play your card](https://bitbucket.org/getsachincode/judgement-card-game/raw/17bf438c8a0c0611978e0d26bcf0ee6fbff5e314/screenshots/play.png) ![Confirm the end of the trick](https://bitbucket.org/getsachincode/judgement-card-game/raw/17bf438c8a0c0611978e0d26bcf0ee6fbff5e314/screenshots/handWinner.png)

- At the end of the round, the total tricks are tallied. The players whose trick count exactly equals their bid, gain 10 points + their bid count in this round. For example if I bid 4 and make exactly 4 tricks, I gain 14 points. If I make 6 tricks, I get 0. Or if I make 2 tricks, I get 0. The players who don't, stay at their previous score.

    >  End of the round. The player names are prefixed by their current position

    >![End of round](https://bitbucket.org/getsachincode/judgement-card-game/raw/17bf438c8a0c0611978e0d26bcf0ee6fbff5e314/screenshots/endOfRound.png)

- In the next round, the trump is changed and each player is now dealt one card less than the last round and the game continues, until the previous round only had one card dealt. To make it interesting, the one card round is repeated so that its played 3 more times (in a 4 player game). Also there is a "no-trump" round every 5th round, where is no strong suit.

- At the end the winner of the game is the one with the highest score.

    >  End of the game. The winner(s) of the game prefixed by ***1***

    >![End of game](https://bitbucket.org/getsachincode/judgement-card-game/raw/17bf438c8a0c0611978e0d26bcf0ee6fbff5e314/screenshots/endOfGame.png)

## Creation of the game:
This Game was created using ***node.js*** for the server with an ***angular*** front end. I used the *socket.io* libraries for client communication and a playing cards css library from *github.com/selfthinker/CSS-Playing-Cards*

## Want to run this game on your own computer?
Install node.js from https://nodejs.org/en/download/ and git from
https://www.atlassian.com/git/tutorials/install-git, if you don't have them already

#### Install the game code
```console
$ git clone https://bitbucket.org/getsachincode/judgement-card-game.git
```
```console
$ cd judgement-card-game
$ npm install
```

#### Use these environment variables as necessary
```console
$ export DEBUG="Judgement*"   # The game application spits log messages prefixed by Judgement
$ export SESSIONSTORAGE=true  # If set to true, you can use multiple browser tabs, each tab is a unique player
$ export START_CARDS=4        # Override the default number of cards dealt in the first round (4 in this case)
$ export PORT=4000            # Override the default port of 3000 (4000 in this case)
```

#### Start the game server
```console
$ npm start
```
Play the game by opening https://localhost:3000


## Future enhancements to the original version (all enhancements completed 6/30/2022):

 1.  Allow the game to be configurable to let 3-8 players play at the same time (Completed 1/24/2022)
 2. Support multiple game tables (Completed 6/30/2022)
 3. If any player drops out (or presses reload), the game terminates. In the future, I will make the game recoverable if that happens. (Completed 11/28/2021)
 4. Port the front end of the game to slack in lieu of the socket.io library

## License

Feel free to download and run this game! Contact *sachinbhajekar@gmail.com* to provide feedback and suggest improvements.

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
