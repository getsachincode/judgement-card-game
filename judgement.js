'use strict'

const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const _ = require('lodash');

const debug = require('debug');
const consoleInfo = debug('Judgement:info');
const consoleError = debug('Judgement:error');

function forceSSL(req, res, next) {
  var herokuProtocolHeader = req.headers['x-forwarded-proto'];
  if (herokuProtocolHeader && herokuProtocolHeader !='https') {
    var hostName = req.get("host");
    hostName = hostName.replace(/:\d+$/, "");
    if (process.env.SSL_PORT) {
      hostName += ":" + process.env.SSL_PORT;
    }
    var redirectURL = 'https://' + hostName + req.url;
    console.log("Redirecting to: " + redirectURL);
    return res.redirect(redirectURL);
  }
  next();
}

if (process.env.APP_ENV != "DEV") {
  app.use(forceSSL);
}

const PORT = process.env.PORT || 3000;
const publicHTMLPath = process.env.NODE_STATIC_HTML || './public';
//Static HTML directory
let responseOptions = {
  setHeaders: (res, path, stat) => {
    res.set('Content-Language', 'en')
  }
};
app.use(express.static(publicHTMLPath, responseOptions));

let sessionStorage = false;
if(process.env.SESSIONSTORAGE && (process.env.SESSIONSTORAGE.toLowerCase() === 'true')) sessionStorage = true;
consoleInfo('Using Session Storage on Browser: ' + sessionStorage)
app.get('/storageOptions.js', (req, res) => {
  res.send(`var SESSIONSTORAGE = ${sessionStorage}`);
});

let boards = [];

// Hard code the number of players for now
// let numPlayers = null;
let numUsers = 0;
let numCards = null;
let currGame = null;
let createBoard = require('./board.js');
const startCards = process.env.START_CARDS;

io.on('connection', (socket) => {
	numUsers++;
  consoleInfo(`a user ${socket.id} connected. Connected users: ${numUsers}`);
  // Move all connection logic to somewhere else
  // This function needs to be updated to remove player's details from the board
  socket.on('disconnect', () => {
  	numUsers--;
    consoleInfo(`user ${socket.id} disconnected. Connected users: ${numUsers}`);
		/*for (let i=0; i<numPlayers; i++) {
			currGame.players[i].cards = [];
		}*/
  });

  socket.on('start game', (initializePlayer) => {
    consoleInfo(`got start message ${JSON.stringify(initializePlayer)}`);
    if (!initializePlayer || (initializePlayer.name == null) || (initializePlayer.numPlayers == null)) {
      let message = `Invalid Request`;
      io.to(socket.id).emit('status', message);
      consoleError('rejecting user');
      io.in(socket.id).disconnectSockets();
      return;
    }

    let gameID = "";
      let idTries = 0;
      while (gameID.length === 0) {
        idTries++;
        for (let i=0; i<4; i++) {
          let alphaNum = "abcdefghijklmnopqrstuvwxyz0123456789".split('');
          let num = Math.floor(Math.random()*36);
          gameID += alphaNum[num];
        }
        if (_.findIndex(boards, {"gameID": gameID}) > -1) {
          if (idTries > 100) {
            consoleInfo(`too many tries, rejecting game`);
            let message = {
              msg: 'Table is full. Try again later',
              state: 'start'
            };
            io.to(socket.id).emit('status', message);
            consoleError('rejecting user');
            io.in(socket.id).disconnectSockets();
            return;
          }
          consoleInfo(`gameID ${gameID} already exists, trying again`);
          gameID = "";
        }
      }
      consoleInfo(`got gameID ${gameID} in ${idTries} tries`);

      boards[boards.length] = createBoard(initializePlayer.numPlayers, gameID);
      let presentGame = boards[boards.length-1];
      if ((startCards > 0) && (startCards < Math.floor(52/initializePlayer.numPlayers))) {
        presentGame.round.numCards = startCards;
      }
      consoleInfo("Board is " + JSON.stringify(presentGame));

      presentGame.players[0].id = socket.id;
      presentGame.players[0].name = initializePlayer.name.substring(0,7);

      let message = {
        msg: `1 player has joined. Waiting for full room.`,
        gameID: gameID,
        displayID: true
      };
      io.to(socket.id).emit('status', message);
  });

  socket.on('load game', (initializePlayer) => {
    consoleInfo(`got load message ${JSON.stringify(initializePlayer)}`);
    if (!initializePlayer || (initializePlayer.name == null)
      || (initializePlayer.gameID == null)) {
      let message = `Invalid Request`;
      io.to(socket.id).emit('status', message);
      consoleError('rejecting user');
      io.in(socket.id).disconnectSockets();
      return;
    }

    let boardNum = _.findIndex(boards, {gameID: initializePlayer.gameID});
    if (boardNum < 0) {
      let message = {
        msg: 'Game Not Found',
        state: 'start'
      };
      io.to(socket.id).emit('status', message);
      consoleError('rejecting user');
      io.in(socket.id).disconnectSockets();
      return;
    }

    let presentGame = boards[boardNum];
    consoleInfo(`found board ${JSON.stringify(presentGame)}`);

    for (let i=0; i<presentGame.players.length;) {
      let positionFound = false;
      if (presentGame.players[i].id === null) {
        presentGame.players[i].id = socket.id;
        // Only allow first 8 characters
        presentGame.players[i].name = initializePlayer.name.substring(0,7);
        positionFound = true;
        consoleInfo("Position Found is " + positionFound);
      }
      if (positionFound) break;
      i++;
      if (i >= presentGame.players.length) {
        let message = {
          msg: 'Table is full. Try again later',
          state: 'start'
        };
        io.to(socket.id).emit('status', message);
        consoleError('rejecting user');
        io.in(socket.id).disconnectSockets();
        return; // Reject the user and exit this function
      }
    }

    let joinedUsers = 0;
    for (let i=0; i<presentGame.players.length; i++) {
      if (presentGame.players[i].name) joinedUsers++;
    }
    consoleInfo(joinedUsers);

    if (joinedUsers < presentGame.players.length) {
      let message = {
        "msg": `${joinedUsers} players have joined. Waiting for full room.`,
        "gameID": initializePlayer.gameID,
        displayID: true
      };
      for (let i=0; i<presentGame.players.length; i++) {
        io.to(presentGame.players[i].id).emit('status', message);
      }
    } else {
      let message = {
        "msg": `${joinedUsers} players have joined. Waiting for game to begin.`,
        "gameID": initializePlayer.gameID
      };
      for (let i=0; i<presentGame.players.length; i++) {
        io.to(presentGame.players[i].id).emit('status', message);
      }
    }
    consoleInfo("Board is " + JSON.stringify(presentGame));
    if (joinedUsers == presentGame.players.length) {
      let message = {
        "msg": "Starting the Game!",
        "state": "wait",
        "gameID": initializePlayer.gameID
      };
      for (let i=0; i<presentGame.players.length; i++) {
        io.to(presentGame.players[i].id).emit('status', message);
      }
      playGame(presentGame);
    }
  });

  socket.on('play card', (playCard) => {
    consoleInfo(`got play message ${JSON.stringify(playCard)}`);
    if (!playCard || (playCard.gameID == null) || (playCard.card == null)) {
      let msg = {
        'msg': 'Card not allowed. Try Another',
        'canType': true,
        'state': 'play'
      };
      io.to(socket.id).emit('status', msg);
      return;
    }
    let gameIndex = _.findIndex(boards, {gameID: playCard.gameID});
    if (gameIndex < 0) {
      let message = {
        msg: 'Game Not Found',
        state: 'start'
      };
      io.to(socket.id).emit('status', message);
      consoleError('rejecting user');
      io.in(socket.id).disconnectSockets();
      return;
    }
    let presentGame = boards[gameIndex];
    let card = parseInt(playCard.card);
    let currPlayer = _.findIndex(presentGame.players, { "id": socket.id });
    if (currPlayer < 0) {
      let message = {
        msg: 'Game Not Found',
        state: 'start'
      };
      io.to(socket.id).emit('status', message);
      consoleError('rejecting user');
      io.in(socket.id).disconnectSockets();
      return;
    }
    consoleInfo('Current Player is ' + JSON.stringify(currPlayer));
  	if (socket.id === presentGame.players[currPlayer].id) {
  		let index = _.indexOf(presentGame.players[currPlayer].cards, card);
  		if ((index === -1) || (!isCardAllowed(card, currPlayer, presentGame))) {
    		let msg = {
  				"msg": 'Card not allowed. Try Another',
  				"canType": true,
  				"state": "play",
          "gameID": presentGame.gameID
  			}

        io.to(socket.id).emit('status', msg);
    	} else {
    		presentGame.players[currPlayer].cards.splice(index, 1);
    		presentGame.round.cards.push(card);
    		consoleInfo(`${presentGame.players[currPlayer].name} played ${card}.`);
    		let msg = {
    			"player": presentGame.players[currPlayer].name,
    			"card": card
    		};
        for (let i=0; i<presentGame.players.length; i++) {
          io.to(presentGame.players[i].id).emit('play card', msg);
        }
    		console.log(`${presentGame.round.cards}, length is ${presentGame.round.cards.length}, numPlayers are ${presentGame.players.length}`);
    		if (presentGame.round.cards.length === 1) {
    			presentGame.round.winningPlayer = currPlayer;
    			presentGame.round.winningCard = card;
    		} else {
    			if (compareCards(card, presentGame.round.winningCard, presentGame.round.trump)) {
    				presentGame.round.winningPlayer = currPlayer;
    				presentGame.round.winningCard = card;
    				consoleInfo(`Winner is ${currPlayer} winning card is ${card}.`);
    			}
    		}
    		if (presentGame.round.cards.length === presentGame.players.length) { // Round is over
          let message = {
            "msg": `${presentGame.players[presentGame.round.winningPlayer].name} won the trick.`,
            "gameID": presentGame.gameID
          };
          for (let i=0; i<presentGame.players.length; i++) {
            io.to(presentGame.players[i].id).emit('status', message);
          }
    			presentGame.players[presentGame.round.winningPlayer].tricks++;
          presentGame.round.handWinner = presentGame.round.winningPlayer;

          // My additions
          presentGame.roundOver = true;
          presentGame.acks = 0;

    			if (presentGame.players[0].cards.length === 0) { // Check for end of round

    				// My additions 
    				presentGame.dealOver = true;
    				presentGame.winners = [];

    				for (let i=0; i<presentGame.players.length; i++) { // No more cards in this round
    					consoleInfo(`for player ${i} bid is ${presentGame.players[i].bid} and tricks are ${presentGame.players[i].tricks}`);
    					if (presentGame.players[i].bid === presentGame.players[i].tricks) {
    						consoleInfo(`adding to score for player ${i}`);
    						presentGame.players[i].score += presentGame.players[i].tricks+10;
    						presentGame.winners.push(presentGame.players[i].name);
    					}
    					presentGame.players[i].tricks = 0;
    					presentGame.players[i].bid = -1;
    				}
    				if (presentGame.round.numCards === 1) {
    					if (presentGame.lastRound == null) {
    						presentGame.lastRound = 0;
    					}
    					presentGame.lastRound ++;
    					if (presentGame.lastRound == presentGame.players.length) { 
    						// My additions
                for (let i=0; i<presentGame.players.length; i++) {
                  io.to(presentGame.players[i].id).emit('status', 'game finished');
                }
    						presentGame.gameOver = true;
    						presentGame.gameWinner = [];
    						let highScore = 0;
    						for (let i=0; i<presentGame.players.length; i++) {
    							if (presentGame.players[i].score > highScore) {
    								highScore = presentGame.players[i].score;
    								presentGame.gameWinner = [];
    								presentGame.gameWinner[0] = presentGame.players[i].name;
    							} else if (presentGame.players[i].score == highScore) {
    								presentGame.gameWinner.push(presentGame.players[i].name);
    							}
    						}
    						presentGame.highScore = highScore;
    						for (let i=0; i<presentGame.players.length; i++) {
    							// For reload purposes
  								presentGame.players[i].status = 'GO'; // Game Over
  								// Reload purposes end
    							playerBoard(presentGame, i);
    							io.to(presentGame.players[i].id).emit('Acknowledge', ' ');
    							io.in(presentGame.players[i].id).disconnectSockets();
    						}

                // Removing game from boards
                consoleInfo('Removing game ' + gameIndex + ' from boards');
                boards.splice(gameIndex, 1);
                consoleInfo('boards are ' + JSON.stringify(boards));
    					} else {
    						presentGame.round.numCards ++; // Deal cards again
    						for (let i=0; i<presentGame.players.length; i++) {
    							// For reload purposes
  								presentGame.players[i].status = 'A'; // Acknowledge
  								// Reload purposes end
    							playerBoard(presentGame, i);
    							io.to(presentGame.players[i].id).emit('Acknowledge', ' ');
    						}
    					}
    				} else {
    					for (let i=0; i<presentGame.players.length; i++) {
    						// For reload purposes
  							presentGame.players[i].status = 'A'; // Acknowledge
  							// Reload purposes end
    						playerBoard(presentGame, i);
    						io.to(presentGame.players[i].id).emit('Acknowledge', ' ');
    					}
    				}
    			} else {
    				for (let i=0; i<presentGame.players.length; i++) {
    					// For reload purposes
  						presentGame.players[i].status = 'A'; // Acknowledge
  						// Reload purposes end
    					playerBoard(presentGame, i);
    					io.to(presentGame.players[i].id).emit('Acknowledge', ' ');
    				}
    			}
   			} else {
   				consoleInfo("Card Played " + card + " and now sending board " + JSON.stringify(presentGame));
   				// For reload purposes
  				presentGame.players[currPlayer].status = 'PD'; // Play Done
  				presentGame.players[(currPlayer+1)%presentGame.players.length].status = 'P'; // Play
  				// Reload purposes end
   				for (let i=0; i<presentGame.players.length; i++) {
    				playerBoard(presentGame, i);
   				}
   				let msg = {
  					"msg": 'Your turn',
  					"canType": true,
  					"state": "play",
            "gameID": presentGame.gameID
  				}
    			io.to(presentGame.players[(currPlayer+1)%presentGame.players.length].id).emit('status', msg);
    		}
    	}
  	}
  	consoleInfo("Board is " + JSON.stringify(presentGame));
  });

  socket.on('bid', (bidMsg) => {
    consoleInfo(`got bid message ${JSON.stringify(bidMsg)}`);
    if (!bidMsg || (bidMsg.gameID == null) || (bidMsg.bid == null)) {
      let msg = {
        'msg': 'Bid not allowed. Try Another',
        'canType': true,
        'state': 'bid'
      };
      io.to(socket.id).emit('status', msg);
      return;
    }
  	let bid = parseInt(bidMsg.bid);
    let index = _.findIndex(boards, {gameID: bidMsg.gameID});
    if (index < 0) {
      let message = {
        msg: 'Game Not Found',
        state: 'start'
      };
      io.to(socket.id).emit('status', message);
      consoleError('rejecting user');
      io.in(socket.id).disconnectSockets();
      return;
    }
    let presentGame = boards[index];

  	let allBid = true;
  	for (let i=0; i<presentGame.players.length; i++) {
  		for (let j=0; j<presentGame.players.length; j++) {
  			if (socket.id === presentGame.players[j].id) {
  				presentGame.players[j].bid = bid;
          let msg = {
            "player": presentGame.players[j].name,
            "bid": bid
          };
          for (let i=0; i<presentGame.players.length; i++) {
            io.to(presentGame.players[i].id).emit('bid', msg);
          }
  			}
  		}
  		playerBoard(presentGame, i);
  		if (presentGame.players[i].bid === -1) {
  			allBid = false;
  		}
  		console.log(`bid is ${bid} and allBid is ${allBid}`);
  	}
  	for (let i=0; i<presentGame.players.length; i++) {
  		if (socket.id === presentGame.players[i].id) {
  			if (allBid) {
  				let bidTotal=0;
  				for (let j=0; j<presentGame.players.length; j++) {
  					bidTotal += presentGame.players[j].bid;
  				}
  				if (bidTotal === presentGame.round.numCards) {
  					presentGame.players[i].bid = -1;
  					let msg = {
  						"msg": 'Bid not allowed. Try Another',
  						"canType": true,
  						"state": "bid",
              "gameID": presentGame.gameID
  					}
  					socket.emit('status', msg);
  				} else {
  					let msg = {
  						"msg": 'Your turn',
  						"canType": true,
  						"state": "play",
              "gameID": presentGame.gameID
  					}
  					// For reload purposes
  					for (let j=0; j<presentGame.players.length; j++) {
  						presentGame.players[j].status = 'PW'; // Play Wait
  						playerBoard(presentGame, j);
  					}
  					presentGame.players[(i+1)%presentGame.players.length].status = 'P'; // Play
  					playerBoard(presentGame, (i+1)%presentGame.players.length);
  					// Reload purposes end
  					io.to(presentGame.players[(i+1)%presentGame.players.length].id).emit('status', msg);
  				}
  			} else {
  				let msg = {
  					"msg": 'Your bid',
  					"canType": true,
  					"state": "bid",
            "gameID": presentGame.gameID
  				}
  				// For reload purposes
  				presentGame.players[i].status = 'BD'; // Bid Done
  				presentGame.players[(i+1)%presentGame.players.length].status = 'B'; // Bid
  				playerBoard(presentGame, (i+1)%presentGame.players.length);
  				playerBoard(presentGame, i);
  				// Reload purposes end
  				io.to(presentGame.players[(i+1)%presentGame.players.length].id).emit('status', msg);
  			}
  		}
 		}
  });

  // Added this function
  socket.on('ack', (ack) => {
    consoleInfo(`got ack message ${JSON.stringify(ack)}`);
    if (!ack || (ack.gameID == null)) {
      let msg = {
        'msg': 'Try again later.',
        'canType': false,
        'state': 'next'
      }
      io.to(socket.id).emit('status', msg);
      return;
    }
    let index = _.findIndex(boards, {gameID: ack.gameID});
    if (index < 0) {
      let message = {
        msg: 'Game Not Found',
        state: 'start'
      };
      io.to(socket.id).emit('status', message);
      consoleError('rejecting user');
      io.in(socket.id).disconnectSockets();
      return;
    }
    let presentGame = boards[index];

  	presentGame.acks++;
  	consoleInfo(`We are now at ${presentGame.acks} Acks`);
  	if (presentGame.acks === presentGame.players.length) {
  		/* Reset the round variables */
  		let whoStarts = presentGame.round.winningPlayer;
  		presentGame.acks = 0;
  		presentGame.roundOver = false;
  		// Setting winningPlayer to previous handWinner
  		presentGame.round.winningPlayer = presentGame.round.handWinner;
    	presentGame.round.winningCard = 0;
    	presentGame.round.cards = [];

  		if (presentGame.dealOver) {
  			// Deal Over reset all deal variables
        presentGame.round.handWinner = 0;
  	 		delete presentGame.winners;
  	 		presentGame.dealOver = false;

        for (let i=0; i<presentGame.players.length; i++) {
          let msg = {
            msg: 'new round',
            gameID: ack.gameID
          };
          io.to(presentGame.players[i].id).emit('status', msg);
        }
    		presentGame.round.starter = (presentGame.round.starter+1)%presentGame.players.length;
    		presentGame.round.winningPlayer = presentGame.round.starter;
    		presentGame.round.handWinner = presentGame.round.winningPlayer;
    		presentGame.round.trump = (presentGame.round.trump+1)%5; // 5 to accomodate NoTrump
    		// For reload purposes
  			for (let j=0; j<presentGame.players.length; j++) {
  				if (j === presentGame.round.starter) {
  					presentGame.players[j].status = 'B'; // Bid
  				} else {
  					presentGame.players[j].status = 'BW'; // Bid Wait
  				}
  				playerBoard(presentGame, j);
  			}
  			// Reload purposes end
    		let msg = {
  				"msg": 'Your bid',
  				"canType": true,
  				"state": "bid",
          "gameID": presentGame.gameID
  			}
    		io.to(presentGame.players[presentGame.round.starter].id).emit('status', msg);
				presentGame.round.numCards--;
				// Don't allow NT to be the trump when there is only one card being dealt
				if (presentGame.round.numCards == 1 && presentGame.round.trump == 4) {
					presentGame.round.trump =0;
				} 
				// Deal New Cards to all
    		playGame(presentGame);
  		} else {
  			// Send updated board back to everybody for the next trick
    		for (let i=0; i<presentGame.players.length; i++) {
    			// For reload purposes
    			if (i === whoStarts) {
    				presentGame.players[i].status = 'P'; // Play
    			} else {
						presentGame.players[i].status = 'PW'; // Bid Wait
					}
					// Reload purposes end
    			playerBoard(presentGame, i);
    		}
  			// Send signal to all whose turn it is
  			let msg = {
 					"msg": 'Your turn',
 					"canType": true,
 					"state": "play",
          "gameID": presentGame.gameID
  			}
				consoleInfo('msg using io.to ' + presentGame.players[whoStarts].id);
				io.to(presentGame.players[whoStarts].id).emit('status', msg);
  		}
  	} else {
  		// For reload purposes
  		let currPlayer = _.findIndex(presentGame.players, { "id": socket.id });
    	if (currPlayer >= 0) {
  			presentGame.players[currPlayer].status = 'AD'; // Ack Done
  		}
  		// Reload purposes end
  	}
  });

  socket.on('reload', (initializePlayer) => {
    consoleInfo("Got " + JSON.stringify(initializePlayer));

  	let gameFound = false;
    if (initializePlayer && (initializePlayer.gameID !== null) && (initializePlayer.playerID !== null)) {
      let index = _.findIndex(boards, {gameID: initializePlayer.gameID});
      if (index < 0) {
        let message = {
          msg: 'Game Not Found',
          state: 'start'
        };
        io.to(socket.id).emit('status', message);
        consoleError('rejecting user');
        io.in(socket.id).disconnectSockets();
        return;
      }
      let presentGame = boards[index];
      consoleInfo(JSON.stringify(presentGame));
      if (presentGame) {
      	let currPlayer = _.findIndex(presentGame.players, { "id": initializePlayer.playerID });
      	if (currPlayer >= 0) {
      		gameFound = true;
      		consoleInfo("Player is " + currPlayer);
      		// replace socketID of player
      		presentGame.players[currPlayer].id = socket.id;

          let boardPlayers = presentGame.players.length;
      		// Send it to player
      		let newBoard = JSON.parse(JSON.stringify(presentGame));
    			// Remove cards
    			for (let j=0; j<presentGame.players.length; j++) {
    				if (currPlayer === j) continue;
    				newBoard.players[j].cards = [];
            delete newBoard.players[j].status;
    			}
    			consoleInfo("Sending Board back " + JSON.stringify(newBoard));
    			io.to(socket.id).emit('reload', newBoard);
    			// If game hasn't begun, dont send the board back
    			if (newBoard.players[currPlayer].status != 'W') {
    				playerBoard(presentGame, currPlayer);
    			} else {
            let gameUsers = 0;
            for (let i=0; i<boardPlayers; i++) {
              if (presentGame.players[i].id !== null) {
                gameUsers++;
              }
            }
            let message;
            if (gameUsers < boardPlayers){
              message = `${gameUsers} player(s) joined. Waiting for full room.`;
            } else {
              message = `${gameUsers} player(s) joined. Waiting for game to begin.`;
            }
            let msg = {
              'state': 'wait',
              displayID: true,
              'msg': message,
              'gameID': presentGame.gameID
            };
            io.to(socket.id).emit('status', msg);
          }
      	}
      }
    }
    if (!gameFound) {
    	// Cant accept this user since this game doesn't exist
    	io.to(socket.id).emit('reload', null);
    	io.in(socket.id).disconnectSockets();
    }
  });

  socket.on('destroy', (destroy) => {
    consoleInfo(`got destroy message ${JSON.stringify(destroy)}`);
    if (!destroy || (destroy.gameID == null)) {
      consoleError('Destroy game request denied');
      return;
    }
    let index = _.findIndex(boards, {gameID: destroy.gameID});
    if (index < 0) {
      io.to(socket.id).emit('destroy', 'Game not found.');
      io.to(socket.id).disconnectSockets();
      return;
    }
    let presentGame = boards[index];

  	consoleInfo(`board is ${JSON.stringify(presentGame)}`);
    let currPlayer = _.findIndex(presentGame.players, { "id": socket.id });
    consoleInfo('Current Player is ' + JSON.stringify(currPlayer));
    if (currPlayer === 0) {
    	let temp = presentGame;
    	boards[index] = null;
    	presentGame = null;
    	consoleInfo("Destroy Game Request Received from Starter");
    	for (let i=0; i<temp.players.length; i++) {
    		io.to(temp.players[i].id).emit('destroy', 'Game destroyed by leader');
        io.in(temp.players[i].id).disconnectSockets();
    	}

      // Removing game from boards
      consoleInfo('Removing game ' + index + ' from boards');
      boards.splice(index, 1);
      consoleInfo('boards are ' + JSON.stringify(boards));
    } else if (currPlayer > 0) {
      if (presentGame.players[currPlayer].status === 'W') {
        presentGame.players[currPlayer] = {
          "name": null,
          "id": null,
          "cards": [],
          "tricks": 0,
          "bid": -1,
          "score": 0,
          // For reload purposes
          "status": 'W'
        };
        io.to(socket.id).emit('destroy', 'You left the game');
        io.to(socket.id).disconnectSockets;

        let gameUsers = 0;
        let boardPlayers = presentGame.players.length;

        for (let i=0; i<boardPlayers; i++) {
          if (presentGame.players[i].id !== null) {
            gameUsers++;
          }
        }
        for (let i=0; i<boardPlayers; i++) {
          if (presentGame.players[i].id !== null) {
            let msg = {
              'msg': `${gameUsers} player(s) joined. Waiting for full room`,
              displayID: true
            }
            io.to(presentGame.players[i].id).emit('status', msg);
          }
        }
      } else {
        io.to(socket.id).emit('destroy', 'Cannot Leave Game Now');
      }
    } else {

    }
  });

});

http.listen(PORT, () => {
  consoleInfo(`listening on: ${PORT}`);
});

// Helper functions

let dealCards = (board, numPlayers) => {
	let arr = [];
	for (let i=0; i<52; i++) {
		arr.push(i);
	}
	let discardCards = 52 - numPlayers*board.round.numCards;
  consoleInfo(discardCards);
	for (let i=52; i>discardCards; i--) {
		let card = arr.splice(Math.floor(Math.random()*i), 1)[0];
		let player = i%numPlayers;
		board.players[player].cards.push(card);
		board.players[player].cards = _.sortBy(board.players[player].cards);
	}
	consoleInfo("Discarded cards " + arr);
	consoleInfo("Dealt cards " + JSON.stringify(board));
	// Added status for reload
	for (let i=0; i<numPlayers; i++) {
  	board.players[i].status = 'BW'; //bid wait
  }
}

let getSuitAndVal = (card) => {
	let suit = Math.floor(card/13);
	let val = card%13+2;
	return {
		"suit": suit,
		"val": val
	};
}

let compareCards = (newCard, oldCard, trump) => {
	let card1 = getSuitAndVal(newCard);
	let card2 = getSuitAndVal(oldCard);
	consoleInfo(`old card: ${oldCard}, new card: ${newCard}`);
	if (card1.suit === card2.suit) {
		if (card1.val > card2.val) {
			return true;
		} else {
			return false;
		}
	} else {
		if (card1.suit === trump) {
			return true;
		} else {
			return false;
		}
	}
}

let playGame = (board) => {
  consoleInfo(board.players.length);
	dealCards(board, board.players.length);
	// Added status for reload
  board.players[board.round.starter].status = 'B';
	for (let i=0; i<board.players.length; i++) {
		playerBoard(board, i);
  }
  let msg = {
  	"msg": 'Your bid',
  	"canType": true,
  	"state": "bid",
    "gameID": board.gameID
  }
  io.to(board.players[board.round.starter].id).emit('status', msg);
}

let playerBoard = (board, player) => {
	let newBoard = JSON.parse(JSON.stringify(board));
  // Remove cards
  for (let j=0; j<board.players.length; j++) {
  	if (player === j) continue;
  	newBoard.players[j].cards = [];
  	delete newBoard.players[j].status;
  }
  // Send it to player
  io.to(board.players[player].id).emit('board', JSON.stringify(newBoard));
  // consoleInfo("Sent Board to " + player + " " + JSON.stringify(newBoard));
}

let isCardAllowed = (card, currPlayer, currGame) => {
	let currPlayerCards = currGame.players[currPlayer].cards;
  let firstCard = currGame.round.cards[0];
  let noCardsInSuit = true;
  if (firstCard != null) {
  	let firstCardSuit = getSuitAndVal(firstCard).suit;
  	let cardSuit = getSuitAndVal(card).suit;
  	if (firstCardSuit != cardSuit) {
  		let minCard = firstCardSuit*13;
  		let maxCard = minCard + 13;
  		for (let i=0; i<currPlayerCards.length; i++) {
  			consoleInfo(`MinMax=${minCard} ${maxCard} ${currPlayerCards[i]}`);
  			if ((currPlayerCards[i]>=minCard) && (currPlayerCards[i]<maxCard)) {
  				//Player is holding a valid card. Disallow this play
  				noCardsInSuit = false;
  				break;
  			}
  		} // Went through all cards in the same suit
  	} // This is the same suit no else needed
  } // This is first card no else needed
  consoleInfo(`Card=${card} firstCard=${firstCard} ${currPlayerCards} noCardsInSuit=${noCardsInSuit}`);
  return noCardsInSuit;
}